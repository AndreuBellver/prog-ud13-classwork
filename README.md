## DCL y DDL Básico Mysql

Conectar a mysql con el usuario por defecto root a través de socket Unix
> sudo mysql;

Crear usuario para la aplicación
> CREATE USER nombre_usuario@'direccion_de_red' IDENTIFIED BY 'password';

Crear base de datos de la aplicación
> CREATE DATABASE nombre_base_de_datos;

Asignar privilegios sobre la base de datos creadas
> GRANT ALL PRIVILEGES ON crm_db.* TO usuario@'direccion_de_red';

Recargar privilegios de usuario
> FLUSH PRIVILEGES;

Cambiar a bases de datos existentes
> USE DATABASE nombre_base_de_datos;

## Importar base de datos a través de script script.sql

> 1. mysql -u nombre_usuario -p'password' < script.sql // con el usuario creado
> 2. mysql < script.sql // con root