package com.bellver.prog.A3;
public class Validator {

    private final String REGEXP_ZIP = "^([0-4][0-9]|5[0-2])\\d{3}$";

    private final String REGEXP_ACCOUNT = "^\\d{4}-\\d{4}-\\d{2}-\\d{10}$";


    private final String REGEXP_DNI = "^\\d{8}[TRWAGMYFPDXBNJZSQVHLCKE]$";


    public final String REGEX_EMAIL = "^[a-z0-9]+(\\.[a-z0-9]+)*@[a-z0-9-_]+(\\.[a-z0-9-_]+)*(\\.([a-z])+)$";


    private final String REGEXP_IP = "^(([1-9]?[0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.){3}([1-9]?[0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$";


    private final String REGEXP_MAC = "^([0-9A-F]{2}[:-]){5}([0-9A-F]{2})$";

    public boolean validateZip(String zip){

        return zip.matches(REGEXP_ZIP);

    }

    public boolean validateAccount(String account){

        return account.matches(REGEXP_ACCOUNT);

    }

    public boolean validateDNI(String dni){

        return dni.matches(REGEXP_DNI);

    }

    public boolean validateEmail(String email){

        return email.matches(REGEX_EMAIL);

    }

    public boolean validateIP(String ipAddress){

        return ipAddress.matches(REGEXP_IP);

    }

    public boolean validateMAC(String macAddress){

        return macAddress.matches(REGEXP_MAC);

    }

    public void validateString(String string, int minLength, int maxLength) {

        if (string.length() < minLength || string.length() > maxLength ) {
            if (minLength == maxLength) {
                throw new InvalidInputException("must be "+ minLength);
            }else{
                throw new InvalidInputException("must be between "+minLength+" and "+maxLength);
            }
        }

    }

}
