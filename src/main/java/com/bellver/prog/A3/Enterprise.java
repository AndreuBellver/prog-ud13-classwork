package com.bellver.prog.A3;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Enterprise {

    private Integer id;
    private String name;
    private String address;
    private String city;
    private String province;
    private String country;
    private String locale;
    private LocalDateTime createdOn;
    private String nif;
    private boolean status;

    public Enterprise(String name, String address, String city, String province,
                      String country, String locale, String nif) {

        this.name = name;
        this.address = address;
        this.city = city;
        this.province = province;
        this.country = country;
        this.locale = locale;
        this.nif = nif;
        this.createdOn = LocalDateTime.now();
        this.status = true;

    }

    public Enterprise(int id, String name, String address, String city, String province, String country,
                      String locale, String nif, LocalDateTime createdOn, boolean status) {

        this.id = id;
        this.name = name;
        this.address = address;
        this.city = city;
        this.province = province;
        this.country = country;
        this.locale = locale;
        this.nif = nif;
        this.createdOn = createdOn;
        this.status = status;

    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public String getCity() {
        return city;
    }

    public String getProvince() {
        return province;
    }

    public String getCountry() {
        return country;
    }

    public String getLocale() {
        return locale;
    }

    public String getCreatedOnISOString() {
        return createdOn.format(DateTimeFormatter.ISO_DATE_TIME);
    }

    public String getNif() {
        return nif;
    }

    public boolean isActive() {
        return status;
    }

    public void setStatus( boolean status){
        this.status = status;
    }

    public void setLocale( String locale){
        this.locale = locale;
    }

    public void setId(int idEnterprise) {
        this.id = idEnterprise;
    }

    @Override
    public String toString() {
        return "Enterprise{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", city='" + city + '\'' +
                ", province='" + province + '\'' +
                ", country='" + country + '\'' +
                ", locale='" + locale + '\'' +
                ", createdOn=" + createdOn +
                ", nif='" + nif + '\'' +
                ", status=" + status +
                '}';
    }
}
