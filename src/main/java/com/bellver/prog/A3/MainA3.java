package com.bellver.prog.A3;

import com.bellver.prog.A1.MySQLConnection;

import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

public class MainA3 {

    public static void main( String[] args ) {

        EnterpriseInputFactory enterpriseInputFactory = new EnterpriseInputFactory(new Validator(), new Scanner(System.in));

        Enterprise enterprise = enterpriseInputFactory.create();

        saveEnterprise(enterprise);

    }

    private static void saveEnterprise(Enterprise enterprise) {

        MySQLConnection mySqlConnection = new MySQLConnection( "crm_db", "andreu", "andreubng96");

        try {

            Statement statement = mySqlConnection.getConnection().createStatement();

            String sql = "INSERT INTO Enterprise(name,address,city,province,country,locale,createdOn,nif,status) ";
            sql += "VALUES(" +
                    "'"+ enterprise.getName() + "'," +
                    "'"+ enterprise.getAddress() + "'," +
                    "'"+ enterprise.getCity() + "'," +
                    "'"+ enterprise.getProvince() + "'," +
                    "'"+ enterprise.getCountry() + "'," +
                    "'"+ enterprise.getLocale() + "'," +
                    "'"+ enterprise.getCreatedOnISOString() + "'," +
                    "'"+ enterprise.getNif() + "'," +
                    "'"+ (enterprise.isActive() ? 1 : 0) + "'"+
                    ")";

            statement.executeUpdate(sql);
            System.out.println("Insertado");

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }


}
