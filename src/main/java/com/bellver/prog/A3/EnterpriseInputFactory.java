package com.bellver.prog.A3;
import java.util.Scanner;

public class EnterpriseInputFactory {

    Validator validator;

    Scanner scanner;

   public EnterpriseInputFactory(Validator validator, Scanner scanner){

        this.scanner = scanner;
        this.validator = validator;

    }

    public Enterprise create(){

        String name = this.getName();
        String address = this.getAddress();
        String city = this.getCity();
        String province = this.getProvince();
        String country = this.getCountry();
        String locale = this.getLocale();
        String nif = this.getNif();

        return new Enterprise(name,address,city,province,country,locale,nif);

    }

    public String getName(){

        String name = this.getAnswer("Introduce el nombre de la empresa");

        this.validator.validateString(name, 1, 45);

        return name;

    }

    public String getAddress(){

        String address = this.getAnswer("Introduce la dirección");

        this.validator.validateString(address, 1, 45);

        return address;

    }

    public String getCity(){

        String city = this.getAnswer("Introduce la ciudad");

        this.validator.validateString(city, 1, 45);

        return city;

    }

    public String getProvince(){

        String province = this.getAnswer("Introduce la provincia");

        this.validator.validateString(province, 1, 45);

        return province;

    }

    public String getCountry(){

        String country = this.getAnswer("Introduce el codigo ISO-3166-2 del país Ej(ES)");

        this.validator.validateString(country, 2, 2);

        return country;

    }

    public String getLocale(){

        String locale = this.getAnswer("Introduce el codigo ISO-15897 del locale (ca_ES)");

        this.validator.validateString(locale, 5, 5);

        return locale;

    }

    private String getNif(){

        String nif = this.getAnswer("Introduce el nif");

        this.validator.validateString(nif, 0, 12);

        return nif;

    }

    private String getAnswer(String question){

        System.out.println(question);
        return this.scanner.nextLine();

    }
}
