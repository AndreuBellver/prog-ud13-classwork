package com.bellver.prog.A3;

public class InvalidInputException extends RuntimeException {

    InvalidInputException(String msg){

        super(msg);

    }
}
