package com.bellver.prog.A9;

import com.bellver.prog.A6.User;

import java.util.ArrayList;

public interface UserRepository {

    public boolean save(User user);

    public User findById(int id);

    public User getById (int id) throws UserNotFoundException;

    public User findByEmail (String email);

    public ArrayList<User> findUserByEnterprise(int idEnterprise);

    public boolean dropUser (int id);
}
