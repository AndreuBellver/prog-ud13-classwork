package com.bellver.prog.A9;

public class UserNotFoundException extends Exception {

    public UserNotFoundException(){

        super("User not found: Error Id");
    }
}
