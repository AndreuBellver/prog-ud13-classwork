package com.bellver.prog.A1;

import com.bellver.prog.A7.DbConnDataReader;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


public class MySQLConnection {

    private Connection connection;

    private String ip;

    private String db;

    private String user;

    private String passowrd;

    private DbConnDataReader dbConnDataReader;

    public MySQLConnection(String ip, String db, String user, String passowrd) {

        this.ip = ip;
        this.db = db;
        this.user = user;
        this.passowrd = passowrd;

    }

    public MySQLConnection(String db, String user, String passowrd) {

        this.ip = "localhost";
        this.db = db;
        this.user = user;
        this.passowrd = passowrd;

    }

    public MySQLConnection(String path) {

        this.dbConnDataReader = new DbConnDataReader();
        dbConnDataReader.getConnectionData(path);

        this.ip = dbConnDataReader.getValue("ip");
        this.db = dbConnDataReader.getValue("database");
        this.user = dbConnDataReader.getValue("user");
        this.passowrd = dbConnDataReader.getValue("password");

    }

    public Connection getConnection() {

        String dbUrl = "jdbc:mysql://" + this.ip + "/" + this.db + "?serverTimezone=UTC";

        try {

            if (connection == null) {
                this.connection = DriverManager.getConnection(dbUrl, user, passowrd);
            }

        } catch (SQLException sqlExcept) {

            System.out.println(sqlExcept.getMessage());
            return null;
        }

        System.out.println("Connexion Enable");
        return connection;

    }

    public void close() {

        try {


            if (!connection.isClosed()) {
                this.connection.close();
            }

        } catch (SQLException sqlException) {

            System.out.println(sqlException.getSQLState());

        }


    }

}
