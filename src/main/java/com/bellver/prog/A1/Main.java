package com.bellver.prog.A1;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

public class Main {

    public static Scanner scanner;

    public static void main (String[] args){

        Connection con = getConnection();

        try {

            Statement statement = con.createStatement();

            if (con.isValid(1000)){
                System.out.print("Connexion establecida\n\n");
            }

        } catch (SQLException ex) {

            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }

    }

    public static Connection getConnection(){

        MySQLConnection mySQLConnection = new MySQLConnection("crm_db","localhost","andreu","andreubng96");


            Connection con = mySQLConnection.getConnection();
            return con;


    }

}
