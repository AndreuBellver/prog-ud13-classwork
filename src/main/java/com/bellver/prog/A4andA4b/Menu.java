package com.bellver.prog.A4andA4b;

import java.util.Scanner;

public class Menu {

    private Scanner scanner;

    public Menu (){

        this.scanner = new Scanner(System.in);

    }

    public int startMenu(){
        System.out.println("Menu:");
        System.out.println("==================");
        System.out.println("");
        System.out.println("");
        System.out.println("1 - Show all Enterprises");
        System.out.println("2 - Update Enterprise");

        return scanner.nextInt();

    }

    public int selectDBID(){

        System.out.println("Cual es la ID de la Empresa que quieres modificar?");

        return scanner.nextInt();
    }

}
