package com.bellver.prog.A4andA4b;

import com.bellver.prog.A1.MySQLConnection;
import com.bellver.prog.A3.Enterprise;
import com.bellver.prog.A3.EnterpriseInputFactory;
import com.bellver.prog.A3.Validator;

import java.sql.*;
import java.util.Scanner;

public class MainA4 {
        public static void main (String[] args){

            Menu menu = new Menu();
            MySQLConnection mySQLConnection = new MySQLConnection
                    ("crm_db","localhost","andreu","andreubng96");

            int election = menu.startMenu();

            if (election == 1){

                Connection connection = mySQLConnection.getConnection();
                String selectAllEnterprises = "SELECT * FROM Enterprise;";

                try {

                    Statement statement = connection.createStatement();

                    ResultSet resultSet = statement.executeQuery(selectAllEnterprises);

                    while (resultSet.next()){

                        System.out.println(resultSet.getInt("id") + " | " +
                                resultSet.getString("name") + " | " +
                                resultSet.getString("address") + " | " +
                                resultSet.getString("city") + " | " +
                                resultSet.getString("province") + " | " +
                                resultSet.getString("country") + " | " +
                                resultSet.getString("locale") + " | " +
                                resultSet.getString("nif"));
                    }

                    resultSet.close();
                    statement.close();
                    connection.close();

                }catch (SQLException e){

                    e.getMessage();
                }

            } else if (election == 2){

                Connection connection = mySQLConnection.getConnection();
                DbEnterpriseRepository dbEnterpriseRepository = new DbEnterpriseRepository(connection);
                Validator validator = new Validator();
                EnterpriseInputFactory enterpriseInputFactory = new EnterpriseInputFactory(validator,
                        new Scanner(System.in));

                int id = menu.selectDBID();

                Enterprise enterprise = dbEnterpriseRepository.findById(id);

                String updateEnterprise = "UPDATE crm_db.Enterprise SET " +
                        "name = '" + enterpriseInputFactory.getName() + "'," +
                        ",address = '" + enterpriseInputFactory.getAddress() + "'," +
                        "city = '" + enterpriseInputFactory.getCity() + "'," +
                        "province = '" + enterpriseInputFactory.getProvince() + "'," +
                        "country = '" + enterpriseInputFactory.getCountry() + "'," +
                        "locale = '" + enterpriseInputFactory.getLocale() + "'," +
                        "where id = " + id + ";";

                try {

                    Statement statement = connection.createStatement();

                    statement.executeUpdate(updateEnterprise);

                    statement.close();
                    connection.close();

                }catch (SQLException e){

                    e.getMessage();
                }



            }

        }

}
