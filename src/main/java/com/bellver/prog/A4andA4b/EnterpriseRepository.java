package com.bellver.prog.A4andA4b;

import com.bellver.prog.A3.Enterprise;

public interface EnterpriseRepository {

    public boolean save(Enterprise enterprise);

    public Enterprise findById (int id);

    public Enterprise getById (int id) throws EnterpriseNotFoundExcetion;
}
