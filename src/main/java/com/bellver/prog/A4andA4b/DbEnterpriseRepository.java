package com.bellver.prog.A4andA4b;

import com.bellver.prog.A3.Enterprise;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DbEnterpriseRepository implements EnterpriseRepository{

    private Connection connection;
    private int pageSize;

    public DbEnterpriseRepository (Connection connection){

        this.connection = connection;
    }

    @Override
    public boolean save(Enterprise enterprise) {

        try {

            Statement statement = connection.createStatement();

            String sql = "INSERT INTO Enterprise(name,address,city,province,country,locale,createdOn,nif,status) ";
            sql += "VALUES(" +
                    "'"+ enterprise.getName() + "'," +
                    "'"+ enterprise.getAddress() + "'," +
                    "'"+ enterprise.getCity() + "'," +
                    "'"+ enterprise.getProvince() + "'," +
                    "'"+ enterprise.getCountry() + "'," +
                    "'"+ enterprise.getLocale() + "'," +
                    "'"+ enterprise.getCreatedOnISOString() + "'," +
                    "'"+ enterprise.getNif() + "'," +
                    "'"+ (enterprise.isActive() ? 1 : 0) + "'"+
                    ")";

            statement.executeUpdate(sql);
            System.out.println("Insertado");
            return true;

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return false;
    }

    @Override
    public Enterprise findById(int id) {

        String query = "Select * from Enterprise;";

        try {

            Statement statement = connection.createStatement();


            ResultSet resultSet = statement.executeQuery(query);



            while (resultSet.next()){

                if (resultSet.getInt("id") == id) {

                    return new Enterprise(
                            resultSet.getString("name"),
                            resultSet.getString("address"),
                            resultSet.getString("city"),
                            resultSet.getString("province"),
                            resultSet.getString("country"),
                            resultSet.getString("locale"),
                            resultSet.getString("nif")
                    );
                }
                            }

        }catch (SQLException e){

            e.getMessage();
        }

        return null;
    }


    @Override
    public Enterprise getById(int id) throws EnterpriseNotFoundExcetion {

        String query = "Select * from Enterprise;";

        try {

            Statement statement = connection.createStatement();


            ResultSet resultSet = statement.executeQuery(query);



            while (resultSet.next()){

                if (resultSet.getInt("id") == id) {

                    return new Enterprise(
                            resultSet.getString("name"),
                            resultSet.getString("address"),
                            resultSet.getString("city"),
                            resultSet.getString("province"),
                            resultSet.getString("country"),
                            resultSet.getString("locale"),
                            resultSet.getString("nif")
                    );
                }

            }

            throw new EnterpriseNotFoundExcetion();

        }catch (SQLException e){

            e.getMessage();

        } catch (EnterpriseNotFoundExcetion ex){

            ex.getMessage();
        }

        return null;

    }
}
