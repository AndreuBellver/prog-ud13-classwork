package com.bellver.prog.A11;

import com.bellver.prog.A11.Catalogue.Catalogue;
import com.bellver.prog.A11.Catalogue.CatalogueMenu;
import com.bellver.prog.A11.WareHouse.OrderMenu;
import com.bellver.prog.A11.WareHouse.WareHouse;

import java.util.Scanner;

public class Enterprise {

    private final int OPTION_CATALOGUE = 1;
    private final int OPTION_WAREHOUSE = 2;
    private final int OPTION_EXIT = 3;

    private CatalogueMenu catalogueMenu;

    private OrderMenu orderMenu;

    private Scanner keyInput;

    public Enterprise(Catalogue defaultCatalogue) {

        WareHouse wareHouse = new WareHouse(defaultCatalogue);
        this.catalogueMenu = new CatalogueMenu(defaultCatalogue);
        this.orderMenu = new OrderMenu(wareHouse);
        this.keyInput = new Scanner(System.in);

    }

    public void show() {

        do {

            int option = this.getUserOption();

            if (option == this.OPTION_CATALOGUE) {

                catalogueMenu.show();

            } else if (option == this.OPTION_WAREHOUSE) {

                orderMenu.show();

            } else if (option == this.OPTION_EXIT) {

                return;

            } else {

                System.out.println("Opción no válida");

            }


        } while (true);


    }

    private int getUserOption() {

        System.out.println(this.OPTION_CATALOGUE + ". Gestionar catálogo productos");
        System.out.println(this.OPTION_WAREHOUSE + ". Gestionar almacén");
        System.out.println(this.OPTION_EXIT + ". Salir");
        return this.keyInput.nextInt();

    }

}
