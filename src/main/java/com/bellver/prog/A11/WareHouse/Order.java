package com.bellver.prog.A11.WareHouse;

import com.bellver.prog.A11.Catalogue.Product;

import java.util.ArrayList;

public class Order {

    private String code;

    private String name;

    private String createdOn;

    private ArrayList<Product> products;

    public Order(String code, String name) {

        this.name = name;
        this.createdOn = "01-02-2019";
        this.code = code;
        this.products = new ArrayList<>();

    }

    public void addNewProduct(Product product) {

        products.add(product);

    }

    @Override
    public String toString() {
        return "Order{" +
                "name='" + name + '\'' +
                ", createdOn='" + createdOn + '\'' +
                ", products=" + products +
                '}';
    }
}
