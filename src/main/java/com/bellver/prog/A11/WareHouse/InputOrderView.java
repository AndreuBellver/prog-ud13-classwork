package com.bellver.prog.A11.WareHouse;

import com.bellver.prog.A11.Catalogue.Catalogue;
import com.bellver.prog.A11.Catalogue.Product;

import java.util.Scanner;

public class InputOrderView {

    private Catalogue catalogue;

    private Scanner keyInput;

    public InputOrderView(Catalogue catalogue) {

        this.keyInput = new Scanner(System.in);
        this.catalogue = catalogue;

    }

    public Order createOrder(String code) {

        Order order = new Order(code, getClientName());
        String response;

        do {

            addNewProduct(order);
            System.out.println("Desea introducir otro producto (S/n)");
            response = keyInput.next();

        } while (response.equalsIgnoreCase("Si"));

        return order;

    }

    private void addNewProduct(Order order) {

        catalogue.listAll();
        System.out.println("Introduzca el código del producto que desea añadir al pedido");
        String code = keyInput.next();
        Product product = catalogue.find(code);

        if (product == null) {
            System.out.println("El código del producto es erroneo");
        }

        try {
            order.addNewProduct(product.clone());
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }

    }

    private String getClientName() {
        char board[][] = {{'X','O',' ',' '},{' ',' ',' ',' '},{' ',' ',' ',' '},{' ',' ',' ',' '}};
        System.out.println("Introduzca su nombre");
        return keyInput.next();

    }

}
