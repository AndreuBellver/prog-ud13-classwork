package com.bellver.prog.A11.WareHouse;

import java.util.Scanner;

public class OrderMenu {

    private final int OPTION_CREATE_ORDER = 1;
    private final int OPTION_LIST = 2;
    private final int OPTION_EXIT = 5;

    private Scanner keyInput;
    private WareHouse wareHouse;

    public OrderMenu(WareHouse wareHouse) {

       this.wareHouse = wareHouse;
       this.keyInput = new Scanner(System.in);

    }

    public void show() {

        do {

            int option = this.getUserOption();

            if (option == this.OPTION_CREATE_ORDER) {

                createOrder();

            } else if (option == this.OPTION_LIST) {

                this.wareHouse.listAll();

            } else if (option == this.OPTION_EXIT) {

                return;

            } else {

                System.out.println("Opción no válida");

            }


        } while (true);


    }

    private int getUserOption() {

        System.out.println(this.OPTION_CREATE_ORDER + ". Crear orden");
        System.out.println(this.OPTION_LIST + ". Listar órdenes");
        System.out.println(this.OPTION_EXIT + ". Salir");
        return this.keyInput.nextInt();

    }

    private void createOrder() {

        this.wareHouse.createOrder();

    }

}
