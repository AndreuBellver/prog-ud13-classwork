package com.bellver.prog.A11.WareHouse;

import com.bellver.prog.A11.Catalogue.Catalogue;

import java.util.ArrayList;

public class WareHouse {

    private ArrayList<Order> orderList;

    private Catalogue catalogue;

    private InputOrderView inputOrderView;

    public WareHouse(Catalogue catalogue) {

        this.orderList = new ArrayList<>();
        this.catalogue = catalogue;
        this.inputOrderView = new InputOrderView(this.catalogue);

    }

    public void createOrder() {

        Order order =  inputOrderView.createOrder(getNextProductCode());
        this.orderList.add(order);

    }

    public void listAll() {

        for (Order order: orderList) {

            System.out.println(order);

        }

    }

    public String getNextProductCode(){

        return "order-" + orderList.size();

    }

}
