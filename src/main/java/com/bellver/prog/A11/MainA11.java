package com.bellver.prog.A11;

import com.bellver.prog.A1.MySQLConnection;
import com.bellver.prog.A11.Catalogue.Catalogue;
import com.bellver.prog.A11.DbController.DbProductRepository;
import com.bellver.prog.A11.DbController.DbShema;


public class MainA11 {

    //public static FileProductRepository fileProductRepository = new FileProductRepository();

    public static MySQLConnection mySQLConnection = new MySQLConnection("dbConfigFile");

    public static DbProductRepository dbProductRepository = new DbProductRepository(mySQLConnection);

    public static DbShema dbShema = new DbShema(mySQLConnection);

    public static void main(String[] args) {

        dbShema.create();

        Enterprise enterprise = new Enterprise(getDefaultCatalogue());
        enterprise.show();

    }

    private static Catalogue getDefaultCatalogue(){

        Catalogue catalogue = new Catalogue(dbProductRepository);

        return catalogue;

    }
}
