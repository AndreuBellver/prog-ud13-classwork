package com.bellver.prog.A11.DbController;

import com.bellver.prog.A1.MySQLConnection;
import com.bellver.prog.A11.CatalogFileRepository.ProductRepository;
import com.bellver.prog.A11.Catalogue.Product;

import java.sql.*;
import java.util.ArrayList;

public class DbProductRepository implements ProductRepository {

    public Connection connection;
    public ArrayList<Product> products;

    public DbProductRepository(MySQLConnection mySQLConnection) {

        this.connection = mySQLConnection.getConnection();
        this.products = new ArrayList<>();

    }

    @Override
    public ArrayList<Product> findAll(){

        String select = "SELECT * FROM Products";

        try {

            Statement statement = connection.createStatement();
            ResultSet resultSet =  statement.executeQuery(select);

            while (resultSet.next()){

                products.add(new Product(
                        resultSet.getString("code"),
                        resultSet.getString("name"),
                        resultSet.getFloat("prize"),
                        resultSet.getFloat("discount"),
                        resultSet.getFloat("vat")
                ));
            }

        }catch (SQLException ex){

            ex.getMessage();
        }

        return products;

    }

    public void saveAll(ArrayList<Product> products){

        for (Product product: products) {

            save(product);
        }
    }

    @Override
    public void save (Product product){

        String insert = "INSERT INTO crm_db.Products(name,prize,discount,vat) VALUES(?,?,?,?)";

        try {

            PreparedStatement preparedStatement = connection.prepareStatement(insert);

            preparedStatement.setString(1,product.getName());
            preparedStatement.setFloat(2,product.getPrize());
            preparedStatement.setFloat(3,product.getDisccount());
            preparedStatement.setFloat(4,product.getVat());

            preparedStatement.executeUpdate();

        }catch (SQLException ex){

            ex.getMessage();
        }

    }

    @Override
    public Product findById(String productId) {
        findAll();

        for ( Product product: products
             ) {

            if (product.getCod() == productId){

                return product;
            }


        }

        return null;

    }

}
