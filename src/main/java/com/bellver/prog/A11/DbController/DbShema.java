package com.bellver.prog.A11.DbController;

import com.bellver.prog.A1.MySQLConnection;
import java.sql.*;
import java.time.LocalDateTime;

public class DbShema {

    private float version;
    private Connection connection;

    public DbShema(MySQLConnection mysqlConnection){

        this.connection = mysqlConnection.getConnection();
        this.version = getVersion();
    }

    public void create(){

        String createTableProduct = "CREATE TABLE IF NOT EXISTS Product(" +
                "code AUTO_INCREMENT PRIMARY KEY, " +
                "name VARCCHAR(45)," +
                "prize FLOAT(8,2)," +
                "discount FLOAT(3,2)," +
                "vat FLOAT(3,2))";

        String createTableVersion = "CREATE TABLE IF NOT EXISTS Version(" +
                "version FLOAT(4,1) AUTO_INCREMENT PRIMARY KEY," +
                "description VARCHAR(160) NOT NULL)";

        try {

            Statement statement = connection.createStatement();

            statement.executeUpdate(createTableProduct);
            statement.executeUpdate(createTableVersion);


        }catch (SQLException ex){

            ex.getMessage();
        }
    }

    public void update(){

     /*
        String insertVersion = "INSERT INTO crm_db.Version(description) VALUES (?)";


        try {

            PreparedStatement preparedStatement = connection.prepareStatement(insertVersion);
            preparedStatement.setString(1, LocalDateTime.now().toString());

        } catch (SQLException ex){

            ex.getMessage();
        }


*/
    }

    private float getVersion(){

     String selectVersion = "Select Version FROM crm_db";

        try {

            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(selectVersion);

            while (resultSet.next()){

                if (resultSet.getInt("Version") > this.version){

                    this.version = resultSet.getInt("Version");
                }
            }

        }catch (SQLException ex) {

            ex.getMessage();
        }

        return this.version;

    }


}
