package com.bellver.prog.A11.Catalogue;

import java.util.Scanner;

public class CatalogueMenu {

    private Catalogue catalogue;
    private InputProductView inputProductView;
    private Scanner keyInput;


    private final int OPTION_REGISTER_NEW = 1;
    private final int OPTION_LIST = 2;
    private final int OPTION_MODIFY = 3;
    private final int OPTION_REMOVE = 4;
    private final int OPTION_EXIT = 5;

    public CatalogueMenu(Catalogue catalogue) {

        this.catalogue = catalogue;
        this.inputProductView = new InputProductView();
        this.keyInput = new Scanner(System.in);

    }

    public void show() {

        do {

            int option = this.getUserOption();

            if (option == this.OPTION_REGISTER_NEW) {

                this.registerNewProduct();

            } else if (option == this.OPTION_LIST) {

                this.catalogue.listAll();

            } else if (option == this.OPTION_MODIFY) {

                this.modifyProduct();

            } else if (option == this.OPTION_REMOVE) {

                this.removeProduct();

            } else if (option == this.OPTION_EXIT) {

                return;

            } else {

                System.out.println("Opción inválida");

            }

        } while (true);

    }

    private int getUserOption() {

        System.out.println(this.OPTION_REGISTER_NEW + ". Añadir Producto");
        System.out.println(this.OPTION_LIST + ". Listar Catálogo");
        System.out.println(this.OPTION_MODIFY + ". Modificar Producto");
        System.out.println(this.OPTION_REMOVE + ". Eliminar Producto");
        System.out.println(this.OPTION_EXIT + ". Salir");
        return this.keyInput.nextInt();

    }

    private void registerNewProduct() {

        String nextProductCode = this.catalogue.getNextProductCode();
        Product product = this.inputProductView.registerNew(nextProductCode);

        this.catalogue.add(product);


    }

    private void removeProduct() {

        String cod = askForProduct();

        if (!this.catalogue.remove(cod)) {
            System.out.println("El producto que desea borrar no existe");
        }

    }

    private void modifyProduct() {

        String cod = askForProduct();
        Product product = this.catalogue.find(cod);

        if (product == null) {
            System.out.println("El producto que desea modificar no existe");
        }

        this.inputProductView.modify(product);

    }

    private String askForProduct() {

        this.catalogue.listAll();
        System.out.println("Seleccione un producto");
        return keyInput.next();

    }

}
