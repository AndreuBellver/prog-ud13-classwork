package com.bellver.prog.A11.Catalogue;

import com.bellver.prog.A1.MySQLConnection;
import com.bellver.prog.A11.CatalogFileRepository.ProductRepository;

import java.util.ArrayList;
import java.util.Scanner;

public class Catalogue {

    private ProductRepository productRepository;

    private ArrayList<Product> products;

    private Scanner keyInput;

    public Catalogue(ProductRepository productRepository) {

        this.keyInput = new Scanner(System.in);
        this.productRepository = productRepository;
        this.products = productRepository.findAll();

    }

    public void add(Product product) {

        products.add(product);

        productRepository.save(product);

    }

    public void listAll() {

        for (Product product : products) {

            System.out.println(product);

        }

    }

    public Product find(String cod) {

        for (Product product : products) {

            if (product.getCod().equals(cod)) {
                return product;
            }

        }

        return null;

    }

    public boolean remove(String cod) {

        Product productToRemove = find(cod);
        if (productToRemove != null) {
            products.remove(productToRemove);
            return true;
        }

        return false;

    }

    public String getNextProductCode() {

        return "SP-" + products.size();

    }


}
