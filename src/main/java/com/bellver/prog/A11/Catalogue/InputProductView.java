package com.bellver.prog.A11.Catalogue;

import java.util.Scanner;

public class InputProductView {

    private Scanner keyInput;

    public InputProductView() {

        this.keyInput = new Scanner(System.in);

    }

    public Product registerNew(String code) {

        Product product = new Product(code, getName(), getPrize(), getDiscount(), getVat());
        return product;

    }

    public void modify(Product product) {

        product.modify(getName(), getPrize(), getDiscount(), getVat());

    }

    private String getName() {

        System.out.println("Introduzca el nombre:");
        return keyInput.next();

    }

    private float getPrize() {

        System.out.println("Introduzca el precio:");
        return keyInput.nextFloat();

    }

    private float getDiscount() {

        System.out.println("Introduzca el descuento:");
        return keyInput.nextFloat();

    }

    private float getVat() {

        System.out.println("Introduzca el IVA:");
        return keyInput.nextFloat();

    }

}
