package com.bellver.prog.A11.Catalogue;

public class Product implements Cloneable{

    private String cod;

    private String name;

    private float prize;

    private float disccount;

    private float vat;

    public String getName() {
        return name;
    }

    public float getDisccount() {
        return disccount;
    }

    public float getVat() {
        return vat;
    }

    public Product(String cod, String name, float prize, float disccount, float vat) {

        this.cod = cod;
        this.name = name;
        this.prize = prize;
        this.disccount = disccount;
        this.vat = vat;

    }

    public String getCod() {

        return this.cod;

    }

    public float getPrize() {

        return prize - (prize*disccount) * (1 + vat);

    }

    public void modify(String name, float prize, float disccount, float vat) {

        this.name = name;
        this.prize = prize;
        this.disccount = disccount;
        this.vat = vat;

    }

    @Override
    public String toString() {
        return "Product{" +
                "cod='" + cod + '\'' +
                ", name='" + name + '\'' +
                ", prize=" + prize +
                ", disccount=" + disccount +
                ", vat=" + vat +
                '}';
    }

    @Override
    public Product clone() throws CloneNotSupportedException {

        return (Product) super.clone();

    }
}
