package com.bellver.prog.A11.CatalogFileRepository;

import com.bellver.prog.A11.Catalogue.Product;

import java.util.ArrayList;

public interface ProductRepository {

    public ArrayList<Product> findAll();

    public void save(Product product);

    public Product findById(String productId);

}
