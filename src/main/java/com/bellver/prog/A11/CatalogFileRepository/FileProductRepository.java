package com.bellver.prog.A11.CatalogFileRepository;

import com.bellver.prog.A11.Catalogue.Product;

import java.io.*;
import java.util.ArrayList;

public class FileProductRepository implements ProductRepository {

    private File catalog;
    private ArrayList<Product> aux = new ArrayList<>();

    public FileProductRepository() {

        this.catalog = new File("resources/a9", "Catalog");

    }

    @Override
    public ArrayList<Product> findAll() {

        String cadena;

        try {

            BufferedReader bufferedReader = new BufferedReader(new FileReader(catalog));

            while ((cadena = bufferedReader.readLine()) != null) {

                aux.add(setProductOfCadena(cadena));

            }

        } catch (IOException e) {

            e.getMessage();

        }


        return aux;

    }

    private Product setProductOfCadena(String cadena) {

        String[] auxiliar = cadena.split(";");

        Product newProduct = new Product(auxiliar[0], auxiliar[1], (Float.valueOf(auxiliar[2])),
                Float.valueOf(auxiliar[3]), Float.valueOf(auxiliar[4]));

        return newProduct;


    }

    public ArrayList<String> getLineasProductos() {

        ArrayList<String> productos = new ArrayList<>();
        String cadena;

        try {

            BufferedReader bufferedReader = new BufferedReader(new FileReader(catalog));

            while ((cadena = bufferedReader.readLine()) != null) {

                productos.add(cadena);

            }

            bufferedReader.close();

        } catch (IOException e) {

            e.getMessage();
        }


        return productos;

    }

    @Override
    public void save(Product product) {

        ArrayList<String> aux = getLineasProductos();

        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(catalog));
            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(catalog));
            PrintWriter printWriter = new PrintWriter(bufferedWriter);


            String newProduct = product.getCod() + ";" + product.getName() + ";" + product.getPrize() + ";" +
                    product.getDisccount() + ";" + product.getVat();

            aux.add(newProduct);

            for (String lineaProducte : aux) {

                printWriter.println(lineaProducte);

            }

            bufferedReader.close();
            bufferedWriter.close();
            printWriter.close();


        } catch (IOException e) {

            e.getMessage();

        }

    }

    @Override
    public Product findById(String productId) {

        findAll();

        for (Product product : aux) {

            if (product.getCod().equalsIgnoreCase(productId)) {

                return product;

            }

        }

        return null;
    }

}
