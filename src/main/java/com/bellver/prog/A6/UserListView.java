package com.bellver.prog.A6;

import com.bellver.prog.A5.PasswordHelper;

import java.sql.Connection;
import java.util.Scanner;

public class UserListView {

    private Scanner scanner;
    private DbUserRepository dbUserRepository;
    private Connection connection;
    private PasswordHelper passwordHelper;

    public UserListView(Connection connection){

        this.scanner = new Scanner(System.in);
        this.dbUserRepository = new DbUserRepository();
        this.passwordHelper = new PasswordHelper();
        this.connection = connection;

    }

    public boolean login(){

        System.out.println("WELCOME TO LOGIN:");
        System.out.println("==================");
        System.out.println();
        System.out.println();

        System.out.println("EMAIL:");
        String email = scanner.next();
        System.out.println();
        System.out.println();
        System.out.println("PASSWORD:");
        String password = scanner.next();

        dbUserRepository.findAll(connection);

        if (dbUserRepository.findByEmail(email) != null){

            if (passwordHelper.isCorrectSha(password,
                    dbUserRepository.findByEmail(email).getPassword())){

                System.out.println("LOGIN ACCEPTADO");
                return true;

            } else {

                System.out.println("PASSWORD INCORRECTA");
                return false;
            }

        } else {

            System.out.println("EMAIL INCORRECTO");
            return false;
        }


    }

}
