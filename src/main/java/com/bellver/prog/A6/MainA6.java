package com.bellver.prog.A6;

import com.bellver.prog.A1.MySQLConnection;

public class MainA6 {

    public static void main(String[] args) {
        MySQLConnection mySQLConnection = new MySQLConnection
                ("crm_db", "localhost", "andreu", "andreubng96");

        UserListView userListView = new UserListView(mySQLConnection.getConnection());

        userListView.login();
    }
}