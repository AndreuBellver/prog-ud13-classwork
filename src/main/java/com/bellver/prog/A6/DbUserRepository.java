package com.bellver.prog.A6;

import com.bellver.prog.A1.MySQLConnection;
import com.bellver.prog.A5.MainA5;
import com.bellver.prog.A9.UserNotFoundException;
import com.bellver.prog.A9.UserRepository;

import java.sql.*;
import java.util.ArrayList;


public class DbUserRepository implements UserRepository {

    private ArrayList<User> users;
    private MySQLConnection mySQLConnection;
    private Connection connection;

    public DbUserRepository() {

        this.users = new ArrayList<>();
        this.mySQLConnection = new MySQLConnection("dbConfigFile/dbConfigFile");
        this.connection = mySQLConnection.getConnection();

    }

    public void saveAll(User[] users){

        for (User user: users) {
            System.out.println("Save");
            save(user);
        }

        MainA5.encryptAll();

        try {

            connection.close();

        } catch (SQLException e){

            e.getMessage();
        }
    }
    @Override
    public boolean save(User user){


        String insert = "INSERT INTO User (firstname, lastName, email, phoneNumber, roles, " +
                "password, active, locale, idEnterprise, birthday) VALUES (?,?,?,?,?,?,?,?,?,?)";

        try {

            PreparedStatement preparedStatement = connection.prepareStatement(insert);

            preparedStatement.setString(1,user.getFirstName());
            preparedStatement.setString(2,user.getLastName());
            preparedStatement.setString(3,user.getEmail());
            preparedStatement.setString(4,user.getPhoneNumber());
            preparedStatement.setString(5,user.getRoles());
            preparedStatement.setString(6,user.getPassword());
            preparedStatement.setInt(7,user.getActive());
            preparedStatement.setString(8,user.getLocale());
            preparedStatement.setInt(9,user.getIdEnterprise());
            preparedStatement.setObject(10,user.getBirthday());


            preparedStatement.executeUpdate();

        }catch (SQLException ex){

            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());

        }

        return true;
    }

    public void findAll(){

        String selectAll = "SELECT * FROM User";

        try {

            Statement statement = connection.createStatement();

            ResultSet resultSet = statement.executeQuery(selectAll);

            while (resultSet.next()){

                users.add(new User(
                        resultSet.getInt("id"),
                        resultSet.getString("firstName"),
                        resultSet.getString("lastName"),
                        resultSet.getString("email"),
                        resultSet.getString("phoneNumber"),
                        resultSet.getString("roles"),
                        resultSet.getString("password"),
                        resultSet.getInt("active"),
                        resultSet.getString("locale"),
                        resultSet.getInt("idEnterprise"),
                        resultSet.getDate("birthday").toLocalDate()
                ));

            }


        }catch (SQLException e){

            e.getMessage();

        }


    }

    @Override
    public User findById(int id){

        findAll();

        for (User user: users) {

            if (id == user.getId()){

                return user;
            }

        }

        return null;
    }

    @Override
    public User getById(int id) throws UserNotFoundException {

        User aux;
        try {

            if ((aux = findById(id)) != null){

                return aux;

            }

            throw new UserNotFoundException();


        } catch (UserNotFoundException e){

            e.getMessage();
        }

        return null;
    }

    public User findByEmail (String email){

        for (User user:users)

                if(user.getEmail().equalsIgnoreCase(email)){

                    return user;
                }

        return null;

    }

    @Override
    public ArrayList<User> findUserByEnterprise(int idEnterprise) {

        ArrayList<User> aux = new ArrayList<>();

        findAll();

        for (User user: users) {

            if (user.getIdEnterprise() == idEnterprise){

                aux.add(user);
            }
        }

        return aux;
    }

    @Override
    public boolean dropUser(int id) {

        String delete = "DELETE FROM crm_db.User where User.id=" + id;

        try {

            Statement statement = connection.createStatement();

            statement.executeUpdate(delete);

        } catch (SQLException ex){

            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());

        }



        return true;
    }

}
