package com.bellver.prog.A6;
import java.time.LocalDate;

public class User {

    private int id;
    private String firstName;
    private String lastName;
    private String email;
    private String phoneNumber;
    private String roles;
    private String password;
    private int active;
    private LocalDate birthday;

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getRoles() {
        return roles;
    }

    public int getActive() {
        return active;
    }

    public String getLocale() {
        return locale;
    }

    public int getIdEnterprise() {
        return idEnterprise;
    }

    private String locale;
    private int idEnterprise;

    public User(String firstName, String lastName, String email, String phoneNumber, String roles,
                String password, int active,
                String locale, int idEnterprise, LocalDate birthday) {

        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.roles = roles;
        this.password = password;
        this.active = active;
        this.locale = locale;
        this.idEnterprise = idEnterprise;
        this.birthday = birthday;

    }

    public User(int id,String firstName, String lastName, String email, String phoneNumber, String roles,
                String password, int active,
                String locale, int idEnterprise, LocalDate birthday) {

        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.roles = roles;
        this.password = password;
        this.active = active;
        this.locale = locale;
        this.idEnterprise = idEnterprise;
        this.birthday = birthday;

    }


    public int getId() {
        return id;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    @Override
    public String toString() {
        return "User{" +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", roles='" + roles + '\'' +
                ", password='" + password + '\'' +
                ", active=" + active +
                ", locale='" + locale + '\'' +
                ", idEnterprise=" + idEnterprise +
                '}';
    }
}
