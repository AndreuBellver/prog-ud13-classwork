package com.bellver.prog.A8;

import com.bellver.prog.A6.DbUserRepository;
import com.bellver.prog.A6.User;
import java.time.Instant;
import java.time.LocalDate;


public class MainA8 {
    public static void main(String[] args){

        User andreu = new User("Andreu", "Bellver Ferrando", "andreu@mail.com",
                "123456789","Manager", "Andreu_Bellver",
                1,"es_ES",1, LocalDate.from(Instant.now()));

        User nico = new User("Nico", "Mengual", "nico@gmail.com",
                "123456789","Manager", "Nico_Mengual",
                1,"es_ES",1, LocalDate.from(Instant.now()));

        User[] newUsers = new User[]{andreu,nico};

        DbUserRepository dbUserRepository = new DbUserRepository();

        dbUserRepository.saveAll(newUsers);

    }
}
