package com.bellver.prog.A7;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;

public class DbConnDataReader {

    private BufferedReader reader;

    private HashMap<String, String> connectionData;


    public DbConnDataReader() {

        this.connectionData = new HashMap<>();

    }

    public HashMap<String, String> getConnectionData(String filePath) {

        String contentLine;
        int contador = 0;

        try {

            this.reader = new BufferedReader(new FileReader(new File(filePath)));

            do {

                contentLine = reader.readLine();

                if (contentLine == null || contentLine == "") {

                    reader.close();
                    break;

                }

                if(contador != 0) {

                    getDataLine(contentLine.replaceAll(" ",""));

                }

                contador++;

            } while (true);

        } catch (IOException io) {

            io.getMessage();

        }

        return connectionData;

    }

    private void getDataLine(String line) {

        String[] linedata = line.split(":");

        this.connectionData.put(linedata[0], linedata[1]);

    }

    public String getValue (String key){

        return this.connectionData.get(key);

    }

}