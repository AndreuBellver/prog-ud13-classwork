package com.bellver.prog.A5;

import org.apache.commons.codec.digest.DigestUtils;

public class PasswordHelper {

    public String getSha1(String password){

        return DigestUtils.sha1Hex(password);
    }

    public boolean isCorrectSha(String password, String sha){

        return (getSha1(password).equalsIgnoreCase(sha));

    }
}
