package com.bellver.prog.A5;

import com.bellver.prog.A1.MySQLConnection;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class MainA5 {
    public static void main(String[] args) {


        encryptAll();

    }

    public static void encryptAll(){
        MySQLConnection mySQLConnection = new MySQLConnection("dbConfigFile/dbConfigFile");

        Connection connection = mySQLConnection.getConnection();
        PasswordHelper passwordHelper = new PasswordHelper();

        try {

            Statement statement = connection.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_UPDATABLE);

            ResultSet resultSet = statement.executeQuery("SELECT * FROM User;");

            while (resultSet.next()) {

                String password = resultSet.getString("firstName") + "_" +
                        resultSet.getString("lastName");

                String passwordHash = passwordHelper.getSha1(password);

                resultSet.updateString("password", passwordHash);
                resultSet.updateRow();

            }

            statement.close();

        } catch (SQLException ex) {

            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());

        }


    }
}
