package com.bellver.prog.A2;

import com.bellver.prog.A1.MySQLConnection;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Main {

        public static void main (String[] args){

            MySQLConnection mySQLConnection = new MySQLConnection("crm_db","localhost","andreu","andreubng96");

            try {

                Connection con = mySQLConnection.getConnection();
                Statement statement = con.createStatement();

                if (con.isValid(1000)){
                    System.out.print("Connexion establecida\n\n");
                }

                ResultSet resultSet = statement.executeQuery("select firstName, lastName, " +
                        "Enterprise.name as Enterprise from Enterprise,User where Enterprise.id = User.idEnterprise " +
                        "and Enterprise.id = 1 or Enterprise.id = 2;\n ");

                while (resultSet.next()){
                    System.out.println(resultSet.getString("firstName") + " " +
                            resultSet.getString("lastName") + " " +
                            resultSet.getString("Enterprise"));

                }

                mySQLConnection.close();

            } catch (SQLException ex) {

                System.out.println("SQLException: " + ex.getMessage());
                System.out.println("SQLState: " + ex.getSQLState());
                System.out.println("VendorError: " + ex.getErrorCode());
            }

    }

}
